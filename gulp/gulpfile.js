const gulp = require('gulp')
const imagemin = require('gulp-imagemin')
const clean = require('gulp-clean')
const concat = require('gulp-concat')
const htmlReplace = require('gulp-html-replace')
const uglify = require('gulp-uglify')
const usemin = require('gulp-usemin')
const cssmin = require('gulp-cssmin')
const browserSync = require('browser-sync').create()
const jshint = require('gulp-jshint')
const jshintStylish = require('jshint-stylish')
const csslint = require('gulp-csslint')
const autoprefixer = require('gulp-autoprefixer')
const less = require('gulp-less')

gulp.task('default', ['copy'], () => {
  gulp.start('build-img', 'usemin')
})

gulp.task('copy', ['clean'], () => {
	return gulp.src('src/**/*')
		.pipe(gulp.dest('dist'))
})

gulp.task('clean', () => {
	return gulp.src('dist')
		.pipe(clean())
})

gulp.task('build-img', () => {
  return gulp.src('dist/img/**/*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/img'))
})

gulp.task('usemin', () => {
  return gulp.src('dist/**/*.html')
    .pipe(usemin({
      js: [uglify],
      css: [autoprefixer]
    }))
    .pipe(gulp.dest('dist'))
})

gulp.task('server', () => {
  browserSync.init({
    server: {
      baseDir: 'src'
    }
  })

  gulp.watch('src/**/*').on('change', browserSync.reload)

  gulp.watch('src/js/**/*.js').on('change', (event) => {
      console.log("Linting " + event.path)
      gulp.src(event.path)
          .pipe(jshint())
          .pipe(jshint.reporter(jshintStylish))
  })

  gulp.watch('src/css/**/*.css').on('change', (event) => {
      gulp.src(event.path)
          .pipe(csslint())
          .pipe(csslint.reporter())
  }) 

  gulp.watch('src/less/**/*.less').on('change', (event) => {
    let stream = gulp.src(event.path)
      .pipe(less().on('error', function(erro) {
        console.log('LESS, erro compilação: ' + erro.filename)
        console.log(erro.message)
      }))
      .pipe(gulp.dest('src/css'))
  })
})
