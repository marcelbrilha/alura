const CorreiosSOAPClient = require('../servicos/CorreiosSOAPClient')

module.exports = (app) => {
	app.post('/correios/calcula-prazo', (req, res) => {
		const dataDelivery = req.body
		const correiosSOAPClient = new CorreiosSOAPClient

		correiosSOAPClient.calculateTerm(dataDelivery, (error, result) => {
			if(error){
				res.status(500).send(error)
				return
			} else {
				res.json(result)
			}
		})
	})
}
