const fs = require('fs')

module.exports = (app) => {
	app.post('/upload/imagem', (req, res) => {
		const filename = req.headers.filename

		req.pipe(fs.createWriteStream('files/' + filename))
			.on('finish', () => {
				res.status(201).send('OK')
			})
	})
}
