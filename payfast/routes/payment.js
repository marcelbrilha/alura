const ConnectionFactory = require('../persistencia/ConnectionFactory')
const PaymentDao = require('../persistencia/PaymentDao')

module.exports = (app) => {
	app.get('/pagamentos', (req, res) => {
		const connection = new ConnectionFactory()
		const paymentDAO = new PaymentDao(connection)

		paymentDAO.list().then(results => {
			res.status(200).send(results)
		})
		.catch(error => {
			res.status(500).send({message: error.toString()})
		})
	})

	app.get('/pagamentos/pagamento/:id', (req, res) => {
		const id = req.params.id
		const connection = new ConnectionFactory()
		const paymentDAO = new PaymentDao(connection)

		paymentDAO.searchId(id).then(results => {
			res.status(200).send(results)
		})
		.catch(error => {
			res.status(500).send({message: error.toString()})
		})
	})

	app.delete('/pagamentos/:id', (req, res) => {
		const id = req.params.id
		const connection = new ConnectionFactory()
		const paymentDAO = new PaymentDao(connection)
		let payment = {}
		payment.id = id
		payment.status = 'Cancelado'

		paymentDAO.update(payment).then(results => {
			res.status(200).send(results)
		})
		.catch(error => {
			res.status(500).send({message: error.toString()})
		})
	})

	app.put('/pagamentos/:id', (req, res) => {
		const id = req.params.id
		const connection = new ConnectionFactory()
		const paymentDAO = new PaymentDao(connection)
		let payment = {}
		payment.id = id
		payment.status = 'Confirmado'

		paymentDAO.update(payment).then(results => {
			res.status(200).send(results)
		})
		.catch(error => {
			res.status(500).send({message: error.toString()})
		})
	})

	app.post('/pagamentos', (req, res) => {
		req.assert("forma_pagamento", "Forma de pagamento é obrigatório").notEmpty()
		req.assert("valor", "Valor é obrigatório e deve ser um decimal").notEmpty().isFloat()

		const errors = req.validationErrors()

		if (errors) {
			res.status(400).send(errors)
			return
		}

		let payment = req.body
		
		payment.status = 'Criado'
		payment.data = new Date

		const connection = new ConnectionFactory()
		const paymentDAO = new PaymentDao(connection)


		paymentDAO.save(payment).then(results => {
			payment.id = result.insertId

			res.location('/pagamentos/' + payment.id)

			const response = {
				dados_pagamento: payment,
				links: [{
					href: "http://localhost:3000/pagamentos/" + payment.id,
					rel: "confirmar",
					method: "PUT"						
				},
				{
					href: "http://localhost:3000/pagamentos/" + payment.id,
					rel: "cancelar",
					method: "DELETE"
				}]
			}
			res.status(200).send(response)
		})
		.catch(error => {
			res.status(500).send({message: error.toString()})
		})
	})
}
