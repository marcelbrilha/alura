const restify = require('restify')

class CartoesClient {
	constructor () {
		this._client = restify.createJsonClient({
			url: 'http://localhost:3000'
		})
	}

	authorizes (cartao, callback) {
		this._client.post('/cartoes/autoriza', cartao, callback)
	}
}

module.exports = CartoesClient
