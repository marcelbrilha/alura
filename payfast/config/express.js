const express = require('express')
const consign = require('consign')
const bodyParser = require('body-parser')
const expressValidator = require('express-validator')
const morgan = require('morgan')
const logger = require('../servicos/logger')

module.exports = () => {
	const app = express()

	app.use(morgan("common", {
		stream: {
			write: message => {
				logger.info(message)
			}
		}
	}))

	app.use(bodyParser.urlencoded({ extended: true }))
	app.use(bodyParser.json())
	app.use(expressValidator())

	consign()
		.include('routes')
		.into(app)

	return app
}
