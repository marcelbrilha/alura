const fs = require('fs')

fs.createReadStream('imagem.jpg')
	.pipe(fs.createWriteStream('imagem-com-stream.jpg'))
	.on('finish', () => {
		console.log('Arquivo escrito com stream')
	})