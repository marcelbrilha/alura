const cluster = require('cluster')
const os = require('os')
const osInfoCPU = os.cpus()

if (cluster.isMaster) {
	osInfoCPU.forEach(() => {
		cluster.fork()
	})

	cluster.on('listening', (worker) => {
		console.log('cluster conectado: ' + worker.process.pid)
	})

	cluster.on('exit', (worker) => {
		console.log('Cluster desconectado: ', worker.process.pid)
		cluster.fork()
	})

} else {
	require('./index')
}
