const mysql = require('mysql')

class ConnectionFactory {
	constructor () {
		return mysql.createConnection({
			host: 'localhost',
			user: 'root',
			password: 'alves85',
			database: 'payfast'
		})
	}
}

module.exports = ConnectionFactory
