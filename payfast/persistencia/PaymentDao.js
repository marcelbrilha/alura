class PaymentDao {
	constructor (connection) {
		this._connection  = connection
	}

	save (payment) {
		return new Promise((resolve, reject) => {
			this._connection.query('INSERT INTO pagamentos SET ?', payment).then(res => {
				resolve(res)
			})
			.catch(err => {
				reject(err)
			})
		})
	}

	update (payment) {
		return new Promise((resolve, reject) => {
			this._connection.query('UPDATE pagamentos SET status = ? where id = ?', [payment.status, payment.id]).then(res => {
				resolve(res)
			})
			.catch(err => {
				reject(err)
			})
		})
	}

	list () {
		return new Promise((resolve, reject) => {
			this._connection.query('SELECT * FROM pagamentos').then(res => {
				resolve(res)
			})
			.catch(err => {
				reject(err)
			})
		})
	}

	searchId (id) {
		return new Promise((resolve, reject) => {
			this._connection.query('SELECT * FROM pagamentos WHERE id = ?', [id]).then(res => {
				resolve(res)
			})
			.catch(err => {
				reject(err)
			})
		})	
	}
}

module.exports = PaymentDao
